const text = process.argv[2];
const figlet = require("figlet");
figlet.text(text, (error, data) => {
    if (error)
        console.log(error);
    else
        console.log(data);
});