const number = process.argv[2] ? process.argv[2] : 8;

let result = '';
for (let i = 1; i <= number; i++) {
    for (let j = 1; j <= number; j++) {
        if (i % 2 == 1) {
            if (j % 2 == 1) {
                result += '##';
            } else {
                result += '  ';
            }
        } else {
            if (j % 2 == 1) {
                result += '  ';
            } else {
                result += '##';
            }
        }
    }
    result += '\n';
}
console.log(result);